import { writable } from 'svelte/store';

export let books = writable([]);

export const fetchBooks = async (title) => {
	const url = `https://eu-central-1.aws.webhooks.mongodb-realm.com/api/client/v2.0/app/application-0-xnjuf/service/http-api/incoming_webhook/webhook0?arg=${title}`;
	const res = await fetch(url);
	const data = await res.json();
	books.set(data);
};