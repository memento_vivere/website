import { writable } from 'svelte/store';

export const pagesList = [
	{
		name: 'Search',
		path: '/'
	},
	{
		name: 'Download All',
		path: '/downloadAll'
	},
	{
		name: 'Find More',
		path: '/findMore'
	},
	{
		name: 'About',
		path: '/about'
	}
];

export let pagePath = writable('/');