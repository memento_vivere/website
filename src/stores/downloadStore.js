export const downloadList = [
	{
		name: 'Colectie Filosofie',
		paths: [
			{
				name: 'A - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/a/-/archive/main/a-main.zip'
			},
			{
				name: 'B - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/b/-/archive/main/b-main.zip'
			},
			{
				name: 'C - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/c/-/archive/main/c-main.zip'
			},
			{
				name: 'D - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/d/-/archive/main/d-main.zip'
			},
			{
				name: 'E - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/e/-/archive/main/e-main.zip'
			},
			{
				name: 'F - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/f/-/archive/main/f-main.zip'
			},
			{
				name: 'G - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/g/-/archive/main/g-main.zip'
			},
			{
				name: 'H - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/h/-/archive/main/h-main.zip'
			},
			{
				name: 'I - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/i/-/archive/main/i-main.zip'
			},
			{
				name: 'J - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/j/-/archive/main/j-main.zip'
			},
			{
				name: 'K - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/k/-/archive/main/k-main.zip'
			},
			{
				name: 'M - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/m/-/archive/main/m-main.zip'
			},
			{
				name: 'N - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/n/-/archive/main/n-main.zip'
			},
			{
				name: 'O - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/o/-/archive/main/o-main.zip'
			},
			{
				name: 'P - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/p/-/archive/main/p-main.zip'
			},
			{
				name: 'R - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/r/-/archive/main/r-main.zip'
			},
			{
				name: 'S - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/s/-/archive/main/s-main.zip'
			},
			{
				name: 'T - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/t/-/archive/main/t-main.zip'
			},
			{
				name: 'U - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/u/-/archive/main/u-main.zip'
			},
			{
				name: 'V - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/v/-/archive/main/v-main.zip'
			},
			{
				name: 'W - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/w/-/archive/main/w-main.zip'
			},
			{
				name: 'Y - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/y/-/archive/main/y-main.zip'
			},
			{
				name: 'Z - Letter',
				link: 'https://gitlab.com/books_1/colectie_filosofie/z/-/archive/main/z-main.zip'
			}
		]
	},

	{
		name: 'Colectie K',
		paths: [
			{
				name: 'A - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/a/-/archive/main/a-main.zip'
			},
			{
				name: 'B - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/b/-/archive/main/b-main.zip'
			},
			{
				name: 'C - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/c/-/archive/main/c-main.zip'
			},
			{
				name: 'D - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/d/-/archive/main/d-main.zip'
			},
			{
				name: 'E - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/e/-/archive/main/e-main.zip'
			},
			{
				name: 'F - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/f/-/archive/main/f-main.zip'
			},
			{
				name: 'G - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/g/-/archive/main/g-main.zip'
			},
			{
				name: 'H - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/h/-/archive/main/h-main.zip'
			},
			{
				name: 'I - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/i/-/archive/main/i-main.zip'
			},
			{
				name: 'J - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/j/-/archive/main/j-main.zip'
			},
			{
				name: 'K - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/k/-/archive/main/k-main.zip'
			},
			{
				name: 'M - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/m/-/archive/main/m-main.zip'
			},
			{
				name: 'N - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/n/-/archive/main/n-main.zip'
			},
			{
				name: 'O - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/o/-/archive/main/o-main.zip'
			},
			{
				name: 'P - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/p/-/archive/main/p-main.zip'
			},
			{
				name: 'R - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/r/-/archive/main/r-main.zip'
			},
			{
				name: 'S - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/s/-/archive/main/s-main.zip'
			},
			{
				name: 'T - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/t/-/archive/main/t-main.zip'
			},
			{
				name: 'U - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/u/-/archive/main/u-main.zip'
			},
			{
				name: 'V - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/v/-/archive/main/v-main.zip'
			},
			{
				name: 'W - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/w/-/archive/main/w-main.zip'
			},
			{
				name: 'X - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/x/-/archive/main/x-main.zip'
			},
			{
				name: 'Y - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/y/-/archive/main/y-main.zip'
			},
			{
				name: 'Z - Letter',
				link: 'https://gitlab.com/books_1/colectie_k/z/-/archive/main/z-main.zip'
			}
		]
	},
	{
		name: 'Colectie Vodafone',
		paths: [
			{
				name: 'Main',
				link: 'https://gitlab.com/books_1/colectie_vodafone/-/archive/main/colectie_vodafone-main.zip'
			}
		]
	}
];
