import { writable, get } from 'svelte/store';
import { browser } from '$app/env';

export const themeList = [
	{
		name: 'Default',
		link: 'https://cdn.jsdelivr.net/npm/bootstrap@5/dist/css/bootstrap.min.css'
	},
	{
		name: 'Flatly',
		link: 'https://cdn.jsdelivr.net/npm/bootswatch@5/dist/flatly/bootstrap.min.css'
	},
	{
		name: 'Darkly',
		link: 'https://cdn.jsdelivr.net/npm/bootswatch@5/dist/darkly/bootstrap.min.css'
	}
];

const key = 'themeDefault';
export let theme = writable((browser && JSON.parse(localStorage.getItem(key))) || themeList[0]);

export function setTheme(theme_item) {
	if (themeList.indexOf(theme_item) !== -1) theme.set(theme_item);
	else theme.set(themeList[0]);
}

theme.subscribe((value) => {
	if (browser) localStorage.setItem(key, JSON.stringify(value));
	return;
});
